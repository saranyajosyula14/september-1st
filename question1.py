def add(num1, num2):
  # finish this function to return a InfiniteNumber by adding self to num2
  if compare(num1,num2) == 1:
    difference_length = len(num1) - len(num2)
    num1 = num1
    num2 = [0]*difference_length + num2

  elif compare(num1,num2) == -1:
    difference_length =  len(num2) - len(num1) 
    num1 = num2
    num2 = [0]*difference_length + num1

  carry = [0]*(len(num1)+1)
  ans = [0]*(len(num1)+1)
  num1 = [0] + num1
  num2 = [0] + num2

  for index in range(len(num1)-1,-1,-1):
    temp = num1[index] + num2[index] + carry[index]
    if len(str(temp)) > 1:
      ans[index] = int(str(temp)[1])
      carry[index-1] = int(str(temp)[0])
    else:
      ans[index] = temp
  for index in range(len(ans)):
    if ans[index] != 0:
      return ans[index::]
  return [0]



def compare(num1, num2):
  # finish this function to 
  # 	return 	1	if num2 is smaller than self
  #		return -1 if num2 is larger than self
  #		return 	0	if second number is equal to self
  if len(num1) > len(num2):
    return 1
  elif len(num2) > len(num1):
    return -1
  else:
    for index in range(len(num1)):
      if num2[index] > num1[index]:
        return -1
      elif num1[index] > num2[index]:
        return 1
  return 0

print(add([2,0,2],[9]))
print(add([1,1],[9]))
print(add([1,9],[1,9]))