from pathlib import Path


f1 = open("assignment.txt", "w")
  
# Open the input file and get 
# the content into a variable data
with open("assignment.txt", "r") as myfile:
    data = myfile.read()
  
# For Full Reversing we will store the 
# value of data into new variable data_1 
# in a reverse order using [start: end: step],
# where step when passed -1 will reverse 
# the string
data1=data[::-1]
print(data1)
p=(Path('/root/assignment.txt').stem)
p1=p[::-1]
print(p1)
